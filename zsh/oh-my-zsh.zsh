# Setup Oh My Zsh
CASE_SENSITIVE=true
HYPHEN_INSENSITIVE=false
ENABLE_CORRECTION=false
COMPLETION_WAITING_DOTS=true

DISABLE_LS_COLORS=false
DISABLE_AUTO_TITLE=false

DISABLE_UNTRACKED_FILES_DIRTY=false
HIST_STAMPS="yyyy-mm-dd"

UPDATE_ZSH_DAYS=7
DISABLE_UPDATE_PROMPT=true
DISABLE_AUTO_UPDATE=false

ZSH_THEME="powerlevel10k/powerlevel10k"

plugins=(sudo extract command-not-found zsh-syntax-highlighting zsh-autosuggestions)

ZSH_AUTOSUGGEST_STRATEGY=(history completion)

source $ZSH/oh-my-zsh.sh
