export HISTSIZE=9223372036854775807
export SAVEHIST=$HISTSIZE

setopt share_history
setopt hist_ignore_all_dups
setopt hist_ignore_space

source "$ZSH_CONFIG/oh-my-zsh.zsh"
source "$ZSH_CONFIG/aliases.zsh"
source "$ZSH_CONFIG/p10k.zsh"
