# Move Oh My Zsh files to a special directory
export ZSH="$HOME/.local/share/oh-my-zsh"

# Move local zsh config files to a special directory
export ZSH_CONFIG="$HOME/.config/zsh"

# Move GnuPG configuration files to a special directory
export GNUPGHOME="$HOME/.config/gnupg"

# Add user's binary files to path
export PATH="$HOME/.local/bin:$PATH"

# Add Rust binaries to PATH
export PATH="$HOME/.local/share/cargo/bin:$PATH"
export CARGO_HOME="$HOME/.local/share/cargo"
export RUSTUP_HOME="$HOME/.local/share/rustup"

# Change default text editor
export EDITOR="/usr/bin/nvim"

# Use exa instead of ls
export LS_PROGRAM="/usr/bin/exa --icons --group"

# Use exa instead of tree
export TREE_PROGRAM="/usr/bin/exa --icons --tree"

# Use bat instead of cat
source /etc/os-release

case $NAME in
  "Ubuntu")
    export CAT_PROGRAM="/usr/bin/batcat --style=plain"
    ;;
  *)
    export CAT_PROGRAM="/usr/bin/bat --style=plain"
    ;;
esac

case $XDG_SESSION_TYPE in
  "x11")
    export CLIPBOARD_COPY_PROGRAM="/usr/bin/xclip -selection clipboard"
    export CLIPBOARD_PASTE_PROGRAM="/usr/bin/xclip -selection clipboard -o"
    ;;
  "wayland")
    export CLIPBOARD_COPY_PROGRAM="/usr/bin/wl-copy"
    export CLIPBOARD_PASTE_PROGRAM="/usr/bin/wl-paste"
    ;;
esac

# Fix cursor theme in Wayland apps
# export XCURSOR_PATH="$HOME/.local/share/icons:$XCURSOR_PATH"

# Disable .NET CLI telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# Use 64-bit Wine prefix hidden in user data directory
export WINEPREFIX="$HOME/.local/share/wine/default.x86_64"
export WINEARCH=win64

# Use ksshaskpass
export SSH_ASKPASS="/usr/bin/ksshaskpass"
export SSH_ASKPASS_REQUIRE=prefer

# Use xdg-desktop-portal-kde
export GTK_USE_PORTAL=1
