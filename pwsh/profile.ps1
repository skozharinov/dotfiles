$oh_my_posh_path = "$env:USERPROFILE\AppData\Local\Programs\oh-my-posh\bin\oh-my-posh.exe"

if (Test-Path -Path $oh_my_posh_path -PathType Leaf) {
  $oh_my_posh_config_path = "$env:USERPROFILE\.omp.json"

  if (Test-Path -Path $oh_my_posh_path -PathType Leaf) {
    & $oh_my_posh_path init pwsh --config $oh_my_posh_config_path | Invoke-Expression
  } else {
    Write-Warning "Missing oh-my-posh config, please place it as '$oh_my_posh_config_path'"
    & $oh_my_posh_path init pwsh | Invoke-Expression
  }
} else {
  Write-Warning "oh-my-posh is missing, type 'winget install oh-my-posh'"
}

New-Alias -Name vs2019pro -Value 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\IDE\devenv.exe'

function l { Get-ChildItem $args | Select-Object Name }
function ll { Get-ChildItem $args }
function lla { Get-ChildItem -Force $args }
New-Alias -Name lt -Value tree

New-Alias -Name c -Value Get-Content

New-Alias -Name e -Value nvim

Remove-Item alias:gc -Force
Remove-Item alias:gp -Force
Remove-Item alias:gl -Force

function g { git $args }
function ga { git add $args }
function gaa { git add --all $args }
function gam { git am $args }
function gap { git apply $args }
function gc { git commit -v $args }
function gcl { git clone --recurse-submodules $args }
function gco { git checkout --recurse-submodules $args }
function gcp { git cherry-pick $args }
function gd { git diff $args }
function gf { git fetch $args }
function gl { git pull $args }
function glg { git log --stat $args }
function glgg { git log --graph $args }
function glgo { git log --oneline $args }
function gp { git push $args }
function gpristine { git reset --hard && git clean -dffx $args }
function gss { git status -s $args }
function gst { git status $args }

Set-PSReadlineOption -EditMode Emacs
Set-PSReadLineOption -PredictionSource History
Set-PSReadLineOption -HistoryNoDuplicates
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete
Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

$env:DOTNET_CLI_TELEMETRY_OPTOUT = 1

Set-Variable MaximumHistoryCount 32767

function load_bat {
  param(
    [Parameter(Position = 0, Mandatory)]
    [string]$BatPath
  )

  cmd /C "$BatPath && pwsh"
}

