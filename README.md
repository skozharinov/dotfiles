# Dotfiles

> This repository is no longer maintained.
> I'm working on actualization of the configuration in a new repository
> that I will publish soon. 

This repository contains a set of configuration files to quickly set up Linux workstation according
to my needs and tastes. It employs [Ansible](https://www.ansible.com/) to automate installation
process. I use [Fedora 35 KDE Plasma Desktop](https://spins.fedoraproject.org/kde/) on my
workstation, other Linux distributions may require additional efforts to install this
configuration.

## What's included?
- [Zsh](https://www.zsh.org/) + [Oh My Zsh](https://ohmyz.sh/) +
[Powerlevel10k](https://github.com/romkatv/powerlevel10k)
- [Neovim](https://neovim.io/) + [Vim-Plug](https://github.com/junegunn/vim-plug)
- [GnuPG](https://gnupg.org/)
- [Git](https://git-scm.com/)
- And a list of software that I want to see on my workstation.

## How to install
Firstly. ensure that you have Ansible installed.
Then run the following command in your terminal:
```
ansible-playbook -K dotfiles.yml
```
The prompt will sho up to ask for your superuser password.
