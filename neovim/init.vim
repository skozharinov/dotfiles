" Change terminal's title
set title

" Hide mode in favour of airline status bar
set noshowmode

" Do not wrap long lines
set nowrap

" Show whitespace characters
set list
set listchars=eol:↲,space:·,trail:~,tab:→/,extends:⟩,precedes:⟨

" Highlight matching brackets
set showmatch

" Show line numbers
set number

" Enable case insensitive search
set ignorecase

" Use 2 spaces for indentation
set tabstop=2
set softtabstop=0
set shiftwidth=0

" Insert spaces instead of tabs
set expandtab

" Enable basic automatic indentation
set autoindent

" Do not mess with cursor
set guicursor=

" Disable mouse support
set mouse=

" Use automatic-indenting depending on file type
filetype plugin indent on

" Enable syntax highlighting
syntax on

" Displays all buffers when there's only one tab open
let g:airline#extensions#tabline#enabled = 1

" Create default mappings
let g:NERDCreateDefaultMappings = 1

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Allow commenting and inverting empty lines
let g:NERDCommentEmptyLines = 1

" Initialize plugin system
" See https://github.com/junegunn/vim-plug
call plug#begin()
" Lean & mean status/tabline for vim that's light as air
Plug 'vim-airline/vim-airline'

" A Vim plugin which shows a git diff in the sign column
Plug 'airblade/vim-gitgutter'

" Vim plugin for intensely nerdy commenting powers
Plug 'scrooloose/nerdcommenter'
call plug#end()

